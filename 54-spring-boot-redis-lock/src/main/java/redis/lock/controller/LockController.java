package redis.lock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import redis.lock.service.LockService;

/**
 * @author guo
 */
@RestController
public class LockController {

    @Autowired
    private LockService lockService;


    @GetMapping("/{key}/{value}/{time}")
    public boolean lock(@PathVariable String key, @PathVariable String value, @PathVariable Long time){
        return lockService.lock(key,value,time);
    }


    @GetMapping("/{key}/{value}")
    public boolean unLock(@PathVariable String key, @PathVariable String value){
        return lockService.unLock(key,value);
    }
}
