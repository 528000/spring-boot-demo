package spring.mail;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringMailApplicationTests {

    @Autowired
	private JavaMailSender javaMailSender;

	@Test
	public void testSimpleSender() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("guoyb1990@163.com");
        simpleMailMessage.setTo("guoyb1990@163.com");
        simpleMailMessage.setSubject("test subject guoyb1990@163.com");
        simpleMailMessage.setText("test text guoyb1990@163.com");
        javaMailSender.send(simpleMailMessage);
	}



	@Test
	public void sendAttachmentsMail() throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom("guoyb1990@163.com");
        mimeMessageHelper.setTo("guoyb1990@163.com");
        mimeMessageHelper.setSubject("test subject guoyb1990@163.com");
        mimeMessageHelper.setText("test text guoyb1990@163.com");

        String filePath = "/Users/guo/IdeaProjects/spring-boot-demo/spring-boot-demo/34-spring-boot-mail/src/main/resources/123.jpeg";
        FileSystemResource fileSystemResource = new FileSystemResource(new File(filePath));
        mimeMessageHelper.addAttachment("附件1.jpg", fileSystemResource);
        mimeMessageHelper.addAttachment("附件2.jpg", fileSystemResource);

        javaMailSender.send(mimeMessage);
    }


    @Test
    public void sendInlineMail() throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom("guoyb1990@163.com");
        mimeMessageHelper.setTo("guoyb1990@163.com");
        mimeMessageHelper.setSubject("静态资源");
        mimeMessageHelper.setText("<html><body><img src='cid:weixin' ></body></html>", true);

        String filePath = "/Users/guo/IdeaProjects/spring-boot-demo/spring-boot-demo/34-spring-boot-mail/src/main/resources/123.jpeg";
        FileSystemResource fileSystemResource = new FileSystemResource(new File(filePath));
        mimeMessageHelper.addInline("weixin", fileSystemResource);

        javaMailSender.send(mimeMessage);
    }


    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @Test
    public void sendTemplateMail() throws MessagingException, IOException, TemplateException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom("guoyb1990@163.com");
        mimeMessageHelper.setTo("guoyb1990@163.com");
        mimeMessageHelper.setSubject("模板邮件");

        Map<String, Object> map = new HashMap<>();
        map.put("username", "blues");
        Template template = freeMarkerConfigurer.getConfiguration().getTemplate("template.html");
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
        mimeMessageHelper.setText(text, true);

        javaMailSender.send(mimeMessage);
    }

}
