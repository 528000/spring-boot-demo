package h2.mapper;

import h2.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface UserMapper {

    @Insert("insert into user(id,name) values(#{id},#{name})")
    public Integer insert(User user);

    @Select("select id,name from user where id=#{id}")
    public User selectById(String id);
}
