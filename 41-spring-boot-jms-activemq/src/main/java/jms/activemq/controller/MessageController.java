package jms.activemq.controller;

import jms.activemq.service.MessageProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    @Autowired
    private MessageProducerService messageProducerService;

    @GetMapping("send/{msg}")
    public String send(@PathVariable String msg){
        messageProducerService.sendMessage(msg);
        return "success";
    }
}
