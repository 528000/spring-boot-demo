package spring.boot.exception.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.boot.exception.common.MyException;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello() throws Exception {
        throw new Exception("wrong wrong wrong");
    }

    @GetMapping("/json")
    public String json() throws MyException {
        throw new MyException("wrong2  wrong2  wrong2");
    }

}
