package spring.boot.small.doc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * hello的类注释
 *
 *
 * @author 郭艺宾
 *
 */
@RestController
public class HelloController {


    /**
     * 打招呼接口
     * @param name 姓名参数
     * @return context 打招呼语句
     */
    @GetMapping("hello/{name}")
    public String hello(@PathVariable(required = false) String name){
        return "hello " + name;
    }

}



