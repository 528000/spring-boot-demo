package transactional.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import transactional.entity.User;
import transactional.mapper.UserMapper;

@Service
@Transactional(readOnly = true,rollbackFor = Exception.class)
public class UserService {


    @Autowired(required = false)
    private UserMapper userMapper;

    @Transactional(readOnly = false,rollbackFor = Exception.class)
    public Integer updates(User blues,User zhangsan) throws Exception{
        int num = userMapper.updateById(blues);

        System.out.println(1/0);

        num = num + userMapper.updateById(zhangsan);
        return num;
    }
}

