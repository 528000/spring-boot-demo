package spring.boot.aop.aop;

import org.springframework.stereotype.Component;

@Component
public @interface AopMethod {
    String value() default "";
}
