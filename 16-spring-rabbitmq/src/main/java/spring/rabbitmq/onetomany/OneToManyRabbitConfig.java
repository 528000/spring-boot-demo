package spring.rabbitmq.onetomany;


import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OneToManyRabbitConfig {

    @Bean
    public Queue oneToManyQueue() {
        return new Queue("onetomany");
    }

}
