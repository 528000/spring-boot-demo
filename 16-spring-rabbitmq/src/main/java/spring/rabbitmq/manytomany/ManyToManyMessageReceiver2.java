package spring.rabbitmq.manytomany;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "manytomany")
public class ManyToManyMessageReceiver2 {

    @RabbitHandler
    public void receiver2(String message) {
        System.out.println("Receiver2  : " + message);
    }

}
