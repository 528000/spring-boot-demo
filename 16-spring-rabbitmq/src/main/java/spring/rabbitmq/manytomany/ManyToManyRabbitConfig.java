package spring.rabbitmq.manytomany;


import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ManyToManyRabbitConfig {

    @Bean
    public Queue manyToManyQueue() {
        return new Queue("manytomany");
    }

}
