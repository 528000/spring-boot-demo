package spring.rabbitmq.onetoone;


import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OneToOneRabbitConfig {

    @Bean
    public Queue oneToOneQueue() {
        return new Queue("message");
    }

}
