package sharding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import sharding.entity.Order;
import sharding.service.OrderService;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;


    @PostMapping("order/{userId}/{orderId}/{name}")
    public Order insert(@PathVariable Long userId, @PathVariable Long orderId, @PathVariable String name){
        Order order = new Order();
        order.setOrderId(orderId);
        order.setUserId(userId);
        order.setName(name);
        return orderService.insert(order);
    }

    @GetMapping("order/{id}")
    public Order selectById(@PathVariable Long id){
        return orderService.selectById(id);
    }
}
