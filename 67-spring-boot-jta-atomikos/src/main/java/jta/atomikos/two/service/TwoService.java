package jta.atomikos.two.service;

import jta.atomikos.model.two.Two;
import jta.atomikos.two.mapper.TwoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author guo
 */
@Service
public class TwoService {

    @Autowired(required = false)
    private TwoMapper twoMapper;


    public Two selectById(String id){
        return twoMapper.selectById(id);
    }
}
