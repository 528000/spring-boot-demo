package consumer.service;

import api.HelloService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {

    @Reference(version = "${spring.application.version}",
            application = "${spring.application.id}",
            registry = "${dubbo.registry.address}")
    private HelloService helloService;

    public String hello() {
        String msg = helloService.hello("Dubbo");
        System.out.println(msg);
        return msg;
    }
}