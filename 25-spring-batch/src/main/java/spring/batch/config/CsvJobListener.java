package spring.batch.config;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * Job监听类
 *
 * @author blues
 */
public class CsvJobListener implements JobExecutionListener{

    private long startTime;
    private long endTime;

    /**
     * job开始前执行
     * @param jobExecution
     */
    @Override
    public void beforeJob(JobExecution jobExecution) {
        startTime = System.currentTimeMillis();
        System.out.println("任务处理开始");
    }

    /**
     * job完成后执行
     * @param jobExecution
     */
    @Override
    public void afterJob(JobExecution jobExecution) {
        endTime = System.currentTimeMillis();
        System.out.println("任务处理结束");
        System.out.println("耗时:" + (endTime - startTime) + " ms ");
    }

}
