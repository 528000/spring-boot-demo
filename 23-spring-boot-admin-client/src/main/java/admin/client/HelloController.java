package admin.client;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guo
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello admin client";
    }
}
