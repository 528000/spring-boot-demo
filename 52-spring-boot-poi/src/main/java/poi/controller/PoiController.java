package poi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import poi.entity.Word;
import poi.service.PoiService;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;

@RestController
public class PoiController {

    @Autowired
    private PoiService poiService;


    @GetMapping("/user")
    public void user(HttpServletResponse response) {
        poiService.user(response);
    }

    @PostMapping("/imports/stream")
    public List<Word> imports(@RequestParam("file") MultipartFile file) throws Exception {
        return poiService.word(file);
    }

    @PostMapping("/import/file")
    public List<Word> imports() {
        File file = new File("./52-spring-boot-poi/src/main/resources/123.xlsx");
        return poiService.word(file);
    }
}
